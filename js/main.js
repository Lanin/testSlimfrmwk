/**
 * Created by Evgeni on 28.12.2015.
 */
$(document).ready(function () {


var rootURL = "http://testslimfmwk.com/api";



$('#findAllBooks').click(function () {
    findAllBooks();
    return false;
});

$('#findAllAuthors').click(function () {
    findAllAuthors();
    return false;
});

$(document).on('change', "input#searchAuthor", function () {
    console.log('searchAuthor by '+this.value);
    $.ajax({
        type: 'GET',
        url: rootURL + '/author/' + this.value,
        dataType: 'json',
        success: renderAuthor
    });
});

$(document).on('change', "input#searchBook", function () {
    console.log('searchBook by '+this.value);
    $.ajax({
        type: 'GET',
        url: rootURL + '/book/' + this.value,
        dataType: 'json',
        success: renderBook
    });
});



function findAllBooks() {
    console.log('findAllBooks');
    $.ajax({
        type: 'GET',
        url: rootURL + '/book',
        dataType: "json", // data type of response
        success: renderBookList
    });
    console.log(rootURL+'/');
}

function findAllAuthors() {
    console.log('findAllAuthors');
    $.ajax({
        type: 'GET',
        url: rootURL + '/author',
        dataType: "json",
        success: renderAuthorList
    });
    console.log(rootURL+'/');
}

function renderBook(data) {
    // JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
    var list = data === null ? [] : (data.books instanceof Array ? data.books : [data.books]);
    console.log(list);

    $('#bookList li').remove();
    $.each(list, function(index, books) {
        $('#bookList').append('<li style="list-style-type:none">'+books.name+'</li>');
    });
}
function renderBookList(data) {
    // JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
    var list = data === null ? [] : (data.books instanceof Array ? data.books : [data.books]);
    console.log(list);

    $('#findAllBooks').hide();
    $('#bookList li').remove();
    $.each(list, function(index, books) {
        $('#bookList').append('<li style="list-style-type:none">'+books.name+'</li>');
    });
}

function renderAuthor(data) {
    var list = data === null ? [] : (data.books instanceof Array ? data.books : [data.books]);
    console.log(list);


    $('#authorList li').remove();
    $.each(list, function(index, authors) {
        $('#authorList').append('<li style="list-style-type:none">'+authors.last_name +" "+ authors.first_name +'</li>');
    });
}

function renderAuthorList(data) {

    var list = data === null ? [] : (data.books instanceof Array ? data.books : [data.books]);
    console.log(list);

    $('#findAllAuthors').hide();
    $('#authorList li').remove();
    $.each(list, function(index, authors) {
        $('#authorList').append('<li style="list-style-type:none">'+authors.last_name +" "+ authors.first_name +'</li>');
    });

}

});