<?php

require 'Slim/Slim.php';
use \Slim\Slim;

\Slim\Slim::registerAutoloader();


$app = new Slim();
$app->config('debug', true);

// GET route
$app->get('/book(/all)', 'getBooks');
$app->get('/book/:id', 'getBookByID');

$app->get('/author(/all)', 'getAuthors');
$app->get('/author/:id', 'getAuthorByID');
// POST route
$app->post(
    '/post',
    function () {
        echo 'This is a POST route';
    }
);

// PUT route
$app->put(
    '/put',
    function () {
        echo 'This is a PUT route';
    }
);

// PATCH route
$app->patch('/patch', function () {
    echo 'This is a PATCH route';
});

// DELETE route
$app->delete(
    '/delete',
    function () {
        echo 'This is a DELETE route';
    }
);


$app->run();

function getBooks()
{
    $sql = "select * FROM book order by name";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $books = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"books": ' . json_encode($books) . '}';
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }

}

function getBookByID($id)
{
    $sql = "select * from book WHERE book_id like :id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $book = $stmt->fetchObject();
        $db = null;
        echo '{"books": ' . json_encode($book) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getAuthors()
{
    $sql = "select * from authors order by last_name";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $author = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"books": ' . json_encode($author) . '}';
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getAuthorByID($id)
{
    $sql = "select * from authors where author_id LIKE :id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $authors = $stmt->fetchObject();
        $db = null;
        echo '{"books": ' . json_encode($authors) . '}';
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}


function getConnection()
{
    $dbhost = "127.0.0.1";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "books";
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}
